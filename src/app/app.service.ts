import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {Dado} from "./modelo/dado.model";

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) { }

  obtenerDados(){
    return this.http.get<Dado>(`${this.URL_BASE}`);
  }

}
