import { Component } from "@angular/core";

@Component({
  selector: 'voltear-carta-frente',
  template: `
  <div class="voltear-carta-frente">
  <ng-content></ng-content>
      </div>
    `,
  styleUrls:['./voltear-carta.component.css']
})
export class VoltearCartaFrenteComponent{}
