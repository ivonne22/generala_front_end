import { Component } from "@angular/core";

@Component({
  selector: 'voltear-carta-reves',
  template: `
  <div class="voltear-carta-reves">
  <ng-content></ng-content>
      </div>
    `,
  styleUrls:['./voltear-carta.component.css']
})
export class VoltearCartaRevesComponent{}
