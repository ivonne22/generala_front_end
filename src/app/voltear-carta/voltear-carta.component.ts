import { Component, OnInit } from '@angular/core';
import { AppService} from "../app.service";

@Component({
  selector: 'app-voltear-carta',
  templateUrl: './voltear-carta.component.html',
  styleUrls: ['./voltear-carta.component.css']
})
export class VoltearCartaComponent implements OnInit {

  toggleProperty = false;
  dados: Array<number> = [];
  jugada: String = "";

  public mensajeError: string = '';


  constructor(private servicio: AppService) { }

  ngOnInit(): void {
    this.servicio.obtenerDados()
      .subscribe((response) => {
          this.dados = response.dados;
          this.jugada = response.jugada;
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });

  }

  toggle() {
    this.toggleProperty = !this.toggleProperty;
    this.servicio.obtenerDados()
      .subscribe((response) => {
          this.dados = response.dados;
          this.jugada = response.jugada;
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });


  }


}
