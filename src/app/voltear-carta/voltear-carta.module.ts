import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoltearCartaComponent } from './voltear-carta.component';
import { VoltearCartaRevesComponent } from "./voltear-carta-reves";
import {VoltearCartaFrenteComponent} from "./voltear-carta-frente";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    VoltearCartaComponent,
    VoltearCartaRevesComponent,
    VoltearCartaFrenteComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
  ],
  exports: [
    VoltearCartaComponent, VoltearCartaRevesComponent, VoltearCartaFrenteComponent
  ]
})
export class VoltearCartaModule { }
